FROM  openjdk:8-alpine

WORKDIR /opt/tomcat

RUN wget https://mirrors.estointernet.in/apache/tomcat/tomcat-8/v8.5.63/bin/apache-tomcat-8.5.63.tar.gz


RUN tar xvfz apache*.tar.gz
RUN mv apache-tomcat-8.5.63/* /opt/tomcat/.
RUN java -version
